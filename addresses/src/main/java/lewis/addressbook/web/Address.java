package lewis.addressbook.web;

public class Address {
	
	private int id;
	private String name;
	private String address;
	private String postCode;
	
	public Address(int id, String name, String address, String postCode) {
		this.name = name;
		this.id = id;
		this.address = address;
		this.postCode = postCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	
}
