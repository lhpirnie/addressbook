package lewis.addressbook.web;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/addressbook")
public class AddressBookController {
	// hello
	@Autowired ResourceLoader loader;

    @RequestMapping(method=RequestMethod.GET)
    public @ResponseBody Dummy sayHello() {
        return new Dummy("Hello World");
    }
    
    @RequestMapping(path="/address/{delegate}", method=RequestMethod.GET)
    public @ResponseBody Address getAddress(@PathVariable(name="delegate") String delegate) {	
        return new Address(12, delegate, "12 Railway Cuttings", "N1 1AA");
    }
    
    @RequestMapping(path="/alladdress", method=RequestMethod.GET)
    public @ResponseBody List<Address> getAllAddress() {
    	List<Address> addresses = new ArrayList<Address>();
    	for (int i=0; i<10; i++) {
    		addresses.add(new Address(i+1, "delegate" + (i+1), "" + (i+13) + " Railway Cuttings", "N1 1AA"));
    	}
        return addresses;
    }
 
    @RequestMapping(path="/addressfile/{delegate}", method=RequestMethod.GET)
    public @ResponseBody Address getAddressFromFile(@PathVariable(name="delegate") String delegate) {
    	Address address = null;
    	try {
    		Resource resource = loader.getResource("classpath:static/" + delegate + ".txt");
    		InputStream is = resource.getInputStream();
    		InputStreamReader isr = new InputStreamReader(is);
    		BufferedReader br = new BufferedReader(isr);
    		int id = Integer.parseInt(delegate.substring(8));
    		String name = br.readLine();
    		String addressString = br.readLine();
    		String postCode = br.readLine();
    		address = new Address(id, name, addressString, postCode);
    		is.close();
    	} catch (Exception e) {
    		System.out.println(e.getMessage());
    		address = new Address(0, "Grim Reaper", "", "");
    	}
        return address;
    }   
}
