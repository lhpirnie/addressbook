package lewis.addressbook.web;

public class Dummy {
	private String message;

	public Dummy(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
