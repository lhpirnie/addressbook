package lewis.addressbook.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AddressBookConfiguration {

	public static void main(String[] args) {
		SpringApplication.run(AddressBookConfiguration.class, args);
	}

}
